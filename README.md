# Per.js
此项目已经启用了码云gitee page服务，使用“ http://per-js.skyogo.com/版本号/Per.js ”链接即可在线获取JS文件<br>
同时欢迎查看和Vue.js的速度对比：https://gitee.com/skyogo/Per.js/blob/master/速度对比VueJS.md
#### 项目介绍
Per.js - 快速、简便的响应式JavaScript开发框架。

- 他可以有效的帮助你减少需要编写的代码量
- 他完全是开源可扩展的
- 他的执行速度几乎是Vue.js的8~7倍

#### 浏览器支持
- IE >= 9
- Chrome >= 4.0
- Firefox >= 4.0
- Safari >= 5.0
- Opera >= 10.0

#### 安装教程

1. 下载Per.js文件
2. 使用script标签引入
3. 完成

#### 使用说明

详情请参见wiki里面的使用方法！

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request